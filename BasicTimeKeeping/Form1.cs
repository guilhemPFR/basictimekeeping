﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasicTimeKeeping
{
    public partial class InputForm : Form
    {
        private const string VoidText = "";
        private const string fileName = "C:\\users\\hrpgxm\\git\\basictimekeeping\\timekeeping.csv";
        
        public InputForm()
        {
            InitializeComponent();
            if (File.Exists(fileName))
            {
                string[] lines = System.IO.File.ReadAllLines(fileName);
                if (lines.Count() > 5)
                {
                    lines = lines.Where((source,index)  => index > lines.Count() - 5).ToArray();
                }
                latestRecordsText.Text = string.Join(Environment.NewLine, lines);
               
            }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if (InputText.Text != VoidText) {
                
                DateTime nowDT = DateTime.Now;
                string newRecord = Environment.NewLine + nowDT.ToString() + "," + InputText.Text;
                if (!File.Exists(fileName))
                {
                    string fileHeader = "DateTime,Job" ;

                    File.WriteAllText(fileName, fileHeader);
                }

                File.AppendAllText(fileName, newRecord);
                InputText.Text = VoidText;
                string[] lines = System.IO.File.ReadAllLines(fileName);
                if (lines.Count() > 5)
                {
                    lines = lines.Where((source, index) => index > lines.Count() - 5).ToArray();
                }
                latestRecordsText.Text = string.Join(Environment.NewLine, lines);
            }
                
        }
    }
}
