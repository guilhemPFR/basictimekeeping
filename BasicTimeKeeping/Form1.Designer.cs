﻿
namespace BasicTimeKeeping
{
    partial class InputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InputText = new System.Windows.Forms.TextBox();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.latestRecordsText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // InputText
            // 
            this.InputText.Location = new System.Drawing.Point(15, 30);
            this.InputText.Name = "InputText";
            this.InputText.Size = new System.Drawing.Size(299, 20);
            this.InputText.TabIndex = 0;
            // 
            // SaveBtn
            // 
            this.SaveBtn.Location = new System.Drawing.Point(15, 66);
            this.SaveBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(61, 23);
            this.SaveBtn.TabIndex = 1;
            this.SaveBtn.Text = "Save";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // latestRecordsText
            // 
            this.latestRecordsText.AutoSize = true;
            this.latestRecordsText.Location = new System.Drawing.Point(12, 103);
            this.latestRecordsText.Name = "latestRecordsText";
            this.latestRecordsText.Size = new System.Drawing.Size(35, 13);
            this.latestRecordsText.TabIndex = 2;
            this.latestRecordsText.Text = "label1";
            // 
            // InputForm
            // 
            this.AcceptButton = this.SaveBtn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 353);
            this.Controls.Add(this.latestRecordsText);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.InputText);
            this.Name = "InputForm";
            this.Text = "Job";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox InputText;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Label latestRecordsText;
    }
}

